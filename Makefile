#/* -*- mode: Makefile; indent-tabs-mode: true; tab-width: 4 -*- */

all:
	cd wolfc/ && $(MAKE)

clean:
	cd wolfc/ && $(MAKE) clean
