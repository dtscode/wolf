=======
Grammar
=======
Wolf Programming Language Grammar::

    letter = "A" | "B" | "C" | "D" | "E" | "F" | "G"
           | "H" | "I" | "J" | "K" | "L" | "M" | "N"
           | "O" | "P" | "Q" | "R" | "S" | "T" | "U"
           | "V" | "W" | "X" | "Y" | "Z" | "a" | "b"
           | "c" | "d" | "e" | "f" | "g" | "h" | "i"
           | "j" | "k" | "l" | "m" | "n" | "o" | "p"
           | "q" | "r" | "s" | "t" | "u" | "v" | "w"
           | "x" | "y" | "z"
    ;

    digit = "0" | "1" | "2" | "3" | "4"
          | "5" | "6" | "7" | "8" | "9"
    ;

    symbol = "[" | "]" | "{" | "}" | "(" | ")" | "<" | ">"
           | "'" | '"' | "=" | "|" | "." | "," | ";"
    ;

    type = "void" | "int32" | "string" | "boolean" ;

    character = letter | digit | symbol | "_" | " " | "\t" | "\n" | "\r" ;
 
    identifier = letter , { letter | digit | "_" } ;

    number = digit , { digit } , [ "." { digit } ] ;

    string = "'" , { character } , "'" ;

    boolean = ( "true" | "false" ) ;

    program = import-list
            | fn-decl
    ;

    import-list = "use" identifier { "," identifier } ";" ;

    fn-decl = "fn" identifier "=" [ identifier [ ":" type ] { "," identifier [ ":" type ] } ] "=>" type "{" { stmt } "}" ;

    stmt = if-chain
         | loop
         | let-stmt ";"
         | ret ";"
         | expr ";"
    ;

    if-chain = "if" expr "{" { stmt } "}" { elif-block } [ else ] ;

    elif-block = "elif" expr "{" { stmt } "}" ;

    else = "else" "{" { stmt } "}" ;

    loop = while-loop
         | foreach-loop
    ;

    while-loop = "while" expr "{" { stmt } "}" ;

    foreach-loop = "foreach" identifier [ ":" type ] "as" identifier "{" { stmt } "}" ;

    let-stmt = "let" identifier [ ":" type ] "=" expr ;

    ret = "ret" expr ;

    expr = expr ( "+" | "-" ) prim
         | prim
    ;

    prim = prim ( "*" | "/" ) boolean
         | boolean
    ;

    conditional = conditional ( "!=" | "==" ) atomic
                | atomic
    ;

    atomic = identifier "(" { expr } ")"
           | "(" { expr } ")"
           | [ "-" ] expr
           | atomic "[" expr "]"
           | identifier
           | number
           | string
           | boolean
    ;
