import System.Environment
import qualified Compiler.Lexer
import qualified Compiler.Parser

main :: IO()
main = do
    [handle] <- getArgs
    code <- readFile handle
    putStrLn (show (Compiler.Parser.parse (Compiler.Lexer.lex code)))
