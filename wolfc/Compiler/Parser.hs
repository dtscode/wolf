module Compiler.Parser (
Compiler.Parser.parse
) where

import qualified Compiler.Lexer (State)

parse :: [(String, Compiler.Lexer.State)] -> String
parse lexemes = (fst (lexemes !! 0))
