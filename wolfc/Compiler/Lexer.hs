module Compiler.Lexer (
  Compiler.Lexer.lex
, State
) where

data State = NONE | WHITE_SPACE | COMMENT | WORD | NUMBER | STRING | SYMBOL deriving (Enum, Eq, Show)

lex :: String -> [(String, State)]
lex "" = []
lex code = tokenize code [] "" NONE

tokenize :: String -> [(String, State)] -> String -> State -> [(String, State)]
tokenize "" lexemes _ _ = lexemes
tokenize (symbol:code_tail) lexemes current_lexeme state
    | state == COMMENT =
        if symbol == '\n' then
            tokenize code_tail lexemes current_lexeme NONE
        else
            tokenize code_tail lexemes current_lexeme state
    | symbol == '\'' =
        if current_lexeme /= "" && state /= STRING then
            (tokenize code_tail (lexemes ++ [(current_lexeme, state)]) "'" STRING)
        else (if state /= STRING then
            tokenize code_tail lexemes "'" STRING
        else
            tokenize code_tail (lexemes ++ [(current_lexeme ++ "'", state)]) "" NONE
        )
    | state == STRING = tokenize code_tail lexemes (current_lexeme ++ [symbol]) state
    | symbol == '#' = 
        if current_lexeme /= "" then
            tokenize code_tail (lexemes ++ [(current_lexeme, state)]) "" COMMENT
        else
            tokenize code_tail lexemes current_lexeme COMMENT
    | symbol `elem` " \t\r\n" =
        if current_lexeme /= "" then
            tokenize code_tail (lexemes ++ [(current_lexeme, state)]) "" WHITE_SPACE
        else
            tokenize code_tail lexemes current_lexeme WHITE_SPACE
    | symbol `elem` ['a'..'z'] || symbol `elem` ['A'..'Z'] || symbol == '_' =
        if current_lexeme /= "" && state /= WORD then
            tokenize code_tail (lexemes ++ [(current_lexeme, state)]) [symbol] WORD
        else
            tokenize code_tail lexemes (current_lexeme ++ [symbol]) WORD
    | symbol `elem` ['0'..'9'] || (symbol == '.' && state == NUMBER) =
        if current_lexeme /= "" && not (state `elem` [WORD, NUMBER]) then
            tokenize code_tail (lexemes ++ [(current_lexeme, state)]) [symbol] NUMBER
        else (if state /= WORD then
            tokenize code_tail lexemes (current_lexeme ++ [symbol]) NUMBER
        else
            tokenize code_tail lexemes (current_lexeme ++ [symbol]) WORD
        )
    | symbol `elem` ";:,=.!+-*/%[]{}()<>" =
        if (current_lexeme ++ [symbol]) `elem` ["=>", "!=", "=="] then
            tokenize code_tail (lexemes ++ [((current_lexeme ++ [symbol]), state)]) "" NONE
        else (if current_lexeme /= "" then
            tokenize code_tail (lexemes ++ [(current_lexeme, state)]) [symbol] SYMBOL
        else
            tokenize code_tail lexemes [symbol] SYMBOL
        )
    | otherwise = tokenize code_tail lexemes current_lexeme state
